#include <vector>
#include <algorithm>
using namespace std;

class Solution {
public:
    int trap(vector<int>& height)
    {
        if (height.size() <= 2) return 0;
        int volcounter = 0;
        // point left, right
        vector<int>::iterator l = height.begin();
        vector<int>::iterator r = height.begin() + height.size()-1;

        // l baseline water, r baseline water
        int lb = *l, rb = *r;
        while(l != r)
        {
            if (lb <= rb)
            {
                l++;
                if (*l < lb)
                {
                    volcounter += (lb - *l);
                    *l = lb;
                }
                // update bottom water level of left
                else //if (*l >= lb)
                    lb = *l;
            }
            else //if (lb > rb)
            {
                r--;
                if (*r < rb)
                {
                    volcounter += (rb - *r);
                    *r = rb;
                }
                //update the bottom layer of right
                else //if (*r >= rb)
                    rb = *r;
            }
        }
        return volcounter;
    }
};


// main
int main()
{
    int a[4] = {2,1,0,2};
    vector<int> input(a, a+4);
    Solution s;
    int res = 0;
    res = s.trap(input);
    return res;
}