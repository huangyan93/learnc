#include <vector>
#include <algorithm>
using namespace std;

// Solution2.2 sort is actually executed
class Solution {
public:
    int InternalReversePairs (vector<int>& nums, int l, int r, vector<int>& sorted)
    {
        if (l >= r) return 0;
        // for example, l = 0, r = 4, mid = 2, lsize = 3, rsize = 2;
        // part1: l--mid, mid - l + 1 elements
        // part2: mid+1--r, r - mid elements
        int mid = (l + r)/2;
        int lsize = mid - l + 1;
        int rsize = r - mid;
        int subRPsL = InternalReversePairs(nums, l, mid, sorted);
        int subRPsR = InternalReversePairs(nums, mid+1, r, sorted);
        int subRPsL2R = 0;
        
        // merge the sorted two subparts
        int indexL = l;
        int indexR = mid+1;
        int indexS = l;
        while ((indexL <= mid) && (indexR <= r))
        {
            // element from part1 is smaller
            if (nums[indexL] <= nums[indexR])
                sorted[indexS++] = nums[indexL++];
            else
            {
                // element from part2 is smaller than the elements from part 1 unused
                // here we only count from the perspective of each element in part2 or part 1
                sorted[indexS++] = nums[indexR++];
                subRPsL2R += (mid - indexL + 1);
            }
        }
        while (indexL <= mid)
            // elements left in part1 (which means these elements are bigger all elements from part2)
            sorted[indexS++] = nums[indexL++];
        while (indexR <= r)
            sorted[indexS++] = nums[indexR++];
        
        // put the merged two sections to substitute original nums
        for (int i = l; i <= r; i++)
            nums[i] = sorted[i];
        return subRPsL+subRPsR+subRPsL2R;
    }
    int reversePairs(vector<int>& nums) 
    {
        if (nums.size() <= 1) return 0;
        vector<int> sorted = nums;
        return InternalReversePairs (nums, 0, nums.size()-1, sorted);
    }
};

// solution2.1, leverage the idea of merge sort, but actually we did not sort the LR subsets
class Solution2_1 {
public:
    int InternalReversePairs (vector<int>& nums, int l, int r)
    {
        if (l >= r) return 0;
        // for example, l = 0, r = 4, mid = 2, lsize = 3, rsize = 2;
        int mid = (l + r)/2;
        int lsize = mid - l + 1;
        int rsize = r - mid;
        int subRPsL = InternalReversePairs(nums, l, mid);
        int subRPsR = InternalReversePairs(nums, mid+1, r);
        int subRPsL2R = 0;
        for (int indexL = l; indexL <= mid; indexL++)
        {
            for (int indexR = mid + 1; indexR <= r; indexR++)
            {
                if (nums[indexL] > nums[indexR])
                    subRPsL2R++;
            }
        }
        return subRPsL+subRPsR+subRPsL2R;

    }
    int reversePairs(vector<int>& nums) {
        if (nums.size() <= 1) return 0;
        return InternalReversePairs (nums, 0, nums.size()-1);
    }
};

// brute force solution, it's too expensive
class Solution1 {
public:
    bool RPexists(vector<vector<int> > arrBuf, int lval, int rval)
    {
        for (int index = 0; index < arrBuf.size(); index++)
        {
            if ((arrBuf[index][0] == lval) && (arrBuf[index][1] == rval))
                return true;
        }
        return false;
    }
    int reversePairs(vector<int>& nums) {
        if (nums.size() <= 1) return 0;
        vector<vector<int> > arrBuf;
        int res = 0;
        for (int left = 0; left < nums.size() - 1; left++)
        {
            for (int right = left + 1; right < nums.size(); right++)
            {
                int lval = nums[left], rval = nums[right];
                if (lval > rval)
                {
                    //judge whether reverPairs already exists
                    //if (RPexists(arrBuf, lval, rval))
                    //    continue;
                    //else
                    {
                        vector<int> tmparr;
                        tmparr.push_back(lval);
                        tmparr.push_back(rval);
                        arrBuf.push_back(tmparr);
                        res++;
                    }
                }
            }
        }
        return res;
    }
};


// main
int main()
{
    int a[4] = {7, 5, 6, 4}; // standard output: 5
    vector<int> input(a, a+4);
    Solution s;
    int res = 0;
    res = s.reversePairs(input);
    return res;
}