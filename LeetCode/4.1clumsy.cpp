#include <stack>
using namespace std;

class Solution {
public:
    int clumsy(int N) {
        if (N == 1) return 1;
        stack<int> s;
        int count = 0; // to control the +-*/ according to its loc
        s.push(N);
        for (int i = N - 1; i >= 1; i--)
        {
            if (count%4 == 0)
            {
                // tmp = s.top();
                // s.pop();
                // s.push(tmp*i);
                s.top() *= i;
            }
            if (count % 4 == 1)
            {
                // tmp = s.top();
                // s.pop();
                // s.push(tmp/i);
                s.top()/=i;
            }
            if (count % 4 == 2)
            {
                s.push(i);
            }
            if (count % 4 == 3)
            {
                s.push(-i);
            }
            count++;
        }
        int sum = 0;
        while(!s.empty())
        {
            sum += s.top();
            s.pop();
        }
        return sum;
    }
};

// main
int main()
{
    Solution s;
    int res = 0;
    res = s.clumsy(4);
    return 0;
}